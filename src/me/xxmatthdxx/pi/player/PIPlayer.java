package me.xxmatthdxx.pi.player;

import me.xxmatthdxx.pi.ProjectInfected;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.UUID;

/**
 * Created by Matt on 19/05/2015.
 */
public class PIPlayer {

    private ProjectInfected plugin = ProjectInfected.getPlugin();

    private Player player;
    private UUID uuid;
    private String name;
    private String lastName;

    private File infoFile;
    private FileConfiguration config;

    public PIPlayer(Player player){
        this.player = player;
        this.uuid = player.getUniqueId();
        name = player.getName();

        if(lastName != name){
            lastName = name;
        }

        infoFile = new File(plugin.getPlayerDir() + "/" + uuid + ".yml");

        if(infoFile.exists()){
            config = YamlConfiguration.loadConfiguration(infoFile);
        }
        else {
            try {
                infoFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            config = YamlConfiguration.loadConfiguration(infoFile);
            setup();
        }
    }

    public void setup(){
        config.set("coins", 0);
        config.set("kills", 0);
        config.set("deaths", 0);
    }

    public void addKill(){
        config.set("kills", getKills() + 1);
        save();
    }

    public int getKills(){
        return config.getInt("kills");
    }

    public void addDeath(){
        config.set("deaths", getDeaths() + 1);
        save();
    }

    public void save(){
        try {
            config.save(infoFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getDeaths(){
        return config.getInt("deaths");
    }

    public double getKDR(){
        DecimalFormat df = new DecimalFormat("00.00");
        double kdr;
        if (getKills() == 0 || getDeaths() == 0) {
            kdr = getKills();
            return Double.valueOf(df.format(kdr));
        } else {
            kdr = getKills() / getDeaths();
            return Double.valueOf(df.format(kdr));
        }
    }

    public void sendMessage(String msg){
        player.sendMessage(msg);
    }
}
