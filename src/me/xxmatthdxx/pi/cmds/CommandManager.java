package me.xxmatthdxx.pi.cmds;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.bukkit.selections.Selection;
import me.xxmatthdxx.pi.ProjectInfected;
import me.xxmatthdxx.pi.game.Arena;
import me.xxmatthdxx.pi.game.GameManager;
import me.xxmatthdxx.pi.game.GameState;
import me.xxmatthdxx.pi.player.PIPlayer;
import me.xxmatthdxx.pi.utils.MessageUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Matthew on 2015-05-17.
 */
public class CommandManager implements CommandExecutor {

    private MessageUtil msg = MessageUtil.getInstance();

    private ProjectInfected plugin = ProjectInfected.getPlugin();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if(cmd.getName().equalsIgnoreCase("game")){
            if(!(sender instanceof Player)){
                sender.sendMessage(ChatColor.RED + "Player only command!");
            }
            else {
                Player pl = (Player) sender;
                if(!pl.hasPermission("pi.game")){
                    pl.sendMessage(ChatColor.RED + "Unknown Command!");
                    return true;
                }

                if(args.length == 0){
                    pl.sendMessage(ChatColor.RED + "/game start");
                    pl.sendMessage(ChatColor.RED + "/game stop");
                    return true;
                }
                else if(args.length == 1){
                    if(args[0].equalsIgnoreCase("start")){
                        if(Bukkit.getOnlinePlayers().size() <= 1){
                            pl.sendMessage(ChatColor.RED + "Not enough players!");
                            return true;
                        }
                        GameManager.getInstance().getMapChosen();
                        GameManager.getInstance().start();
                    }
                    else if(args[0].equalsIgnoreCase("stop")){
                        GameManager.getInstance().stop(false);
                    }
                    else if(args[0].equalsIgnoreCase("disable")){
                        ProjectInfected.getPlugin().setState(GameState.OFFLINE);
                        ProjectInfected.getPlugin().setTicks(Integer.MAX_VALUE);
                    }
                }
                else if(args.length == 2){
                    if(args[0].equalsIgnoreCase("create")){
                        String name = args[1];

                        if(GameManager.getInstance().getArena(name) != null){
                            pl.sendMessage(msg.getMessageFromConfig("arena_exists"));
                            return true;
                        }

                        Location loc = pl.getLocation();

                        Selection sel = getWorldEdit().getSelection(pl);

                        if(sel == null){
                            pl.sendMessage(msg.getMessageFromConfig("no_selection"));
                            return true;
                        }

                        Location p1 = sel.getMinimumPoint();
                        Location p2 = sel.getMaximumPoint();

                        plugin.getConfig().set("arenas." + name + ".spawn", loc.getX() + "," + loc.getY() + "," + loc.getZ() + "," + loc.getWorld().getName());
                        plugin.saveConfig();

                        plugin.getConfig().set("arenas." + name + ".p1", p1.getX() + "," + p1.getY() + "," + p1.getZ() + "," + p1.getWorld().getName());
                        plugin.saveConfig();

                        plugin.getConfig().set("arenas." + name + ".p2", p2.getX() + "," + p2.getY() + "," + p2.getZ() + "," + p2.getWorld().getName());
                        plugin.saveConfig();
                        plugin.reloadConfig();
                        Arena a = new Arena(name,loc, p1,p2);
                        GameManager.getInstance().getArenas().add(a);
                        pl.sendMessage(msg.getMessageFromConfig("create_arena").replace("{ARENA}", name));
                    }
                    else if(args[0].equalsIgnoreCase("delete")){
                        String name = args[1];

                        if(GameManager.getInstance().getArena(name) == null){
                            pl.sendMessage(ChatColor.RED + "No arena in memory!");
                            return true;
                        }
                        else {

                            if(GameManager.getInstance().getCurrentArena() == GameManager.getInstance().getArena(name)){
                                pl.sendMessage(ChatColor.RED + "Game is in progress, please wait!");
                                return true;
                            }

                            plugin.getConfig().set("arenas." + name, null);
                            plugin.saveConfig();
                            plugin.reloadConfig();
                            GameManager.getInstance().getArenas().remove(GameManager.getInstance().getArena(name));
                            pl.sendMessage(ChatColor.RED + "Destroyed arena!");
                            return true;
                        }
                    }
                }
            }
        }
        else if(cmd.getName().equalsIgnoreCase("vote")){
            if(!(sender instanceof Player)){
                return true;
            }
            Player pl = (Player) sender;

            if(ProjectInfected.getPlugin().getState() != GameState.VOTING){
                pl.sendMessage(ChatColor.RED + "You cannot vote at this time!");
                return true;
            }

            if(args.length == 0){
                for(Arena a : GameManager.getInstance().getArenas()){
                    pl.sendMessage(a.getName());
                }
            }
            else if(args.length == 1){
                String name = args[0];

                if(GameManager.getInstance().getArena(name) == null){
                    pl.sendMessage(ChatColor.RED + "No arena with that name!");
                    return true;
                }
                else {
                    Arena a = GameManager.getInstance().getArena(name);
                    if(GameManager.getInstance().getPlayerVotes().containsKey(pl.getUniqueId())){
                        Arena toRemove = GameManager.getInstance().getPlayerVotes().get(pl.getUniqueId());
                        toRemove.removeVote();
                        a.addVote();
                        GameManager.getInstance().getPlayerVotes().remove(pl.getUniqueId());
                        GameManager.getInstance().getPlayerVotes().put(pl.getUniqueId(), a);
                        pl.sendMessage(msg.getMessageFromConfig("changed_vote").replace("{ARENA}", name));
                    }
                    GameManager.getInstance().getPlayerVotes().put(pl.getUniqueId(), a);
                    a.addVote();
                    pl.sendMessage(msg.getMessageFromConfig("voted").replace("{ARENA}", name));
                }
            }
        }
        else if(cmd.getName().equalsIgnoreCase("stats")){
            if(!(sender instanceof Player)){
                sender.sendMessage(ChatColor.RED + "Player only command");
                return true;
            }

            Player pl = (Player) sender;
            if(args.length == 0){
                PIPlayer localPlayer = new PIPlayer(pl);
                localPlayer.sendMessage(msg.getMessageFromConfig("kills_stats_format").replace("{KILLS}", localPlayer.getKills() + ""));
                localPlayer.sendMessage(msg.getMessageFromConfig("deaths_stats_format").replace("{DEATHS}", localPlayer.getDeaths() + ""));
                localPlayer.sendMessage(msg.getMessageFromConfig("kdr_stats_format").replace("{KDR}", localPlayer.getKDR() + ""));
                return true;
            }
            else {
                pl.sendMessage(ChatColor.RED + "Searching other players stats is being implemented soon!");
                return true;
            }
        }
        return false;
    }

    public WorldEditPlugin getWorldEdit(){
        return (WorldEditPlugin) Bukkit.getPluginManager().getPlugin("WorldEdit");
    }
}
