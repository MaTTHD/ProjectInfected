package me.xxmatthdxx.pi.listeners;

import me.xxmatthdxx.pi.game.GameManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * Created by Matthew on 2015-05-17.
 */
public class PlayerDamage implements Listener {

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        if (!(e.getEntity() instanceof Player)) {
            return;
        }
        Player victim = (Player) e.getEntity();

        if (e.getCause() == EntityDamageEvent.DamageCause.VOID) {
            victim.setHealth(victim.getMaxHealth());
            if (GameManager.getInstance().getInfected().contains(victim.getUniqueId())) {
                victim.teleport(GameManager.getInstance().getCurrentArena().getSpawn());
            } else if (GameManager.getInstance().getAlive().contains(victim.getUniqueId())) {
                GameManager.getInstance().addInfected(victim);
                if (GameManager.getInstance().canContinue()) {
                } else {
                    GameManager.getInstance().stop(true);
                }
            }
        }
        if (victim.getHealth() - e.getDamage() <= 0) {
            victim.setHealth(victim.getMaxHealth());
            if (GameManager.getInstance().getInfected().contains(victim.getUniqueId())) {
                victim.teleport(GameManager.getInstance().getCurrentArena().getSpawn());
            } else if (GameManager.getInstance().getAlive().contains(victim.getUniqueId())) {
                GameManager.getInstance().addInfected(victim);
                if (GameManager.getInstance().canContinue()) {
                } else {
                    GameManager.getInstance().stop(true);
                }
            }
        }
        return;
    }
}