package me.xxmatthdxx.pi.listeners;

import me.xxmatthdxx.pi.ProjectInfected;
import me.xxmatthdxx.pi.game.GameManager;
import me.xxmatthdxx.pi.game.GameState;
import me.xxmatthdxx.pi.player.PIPlayer;
import me.xxmatthdxx.pi.utils.MessageUtil;
import me.xxmatthdxx.pi.utils.ScoreBoardUtils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Created by Matthew on 2015-05-17.
 */
public class PlayerJoin implements Listener {

    private ProjectInfected plugin = ProjectInfected.getPlugin();
    private GameManager game = GameManager.getInstance();
    private MessageUtil msg = MessageUtil.getInstance();

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        PIPlayer piPlayer = new PIPlayer(e.getPlayer());

        if (plugin.getState() == GameState.IN_GAME) {
            game.addInfected(e.getPlayer());
            e.getPlayer().teleport(GameManager.getInstance().getCurrentArena().getSpawn());
        } else {
            e.setJoinMessage(msg.getMessageFromConfig("join_message").replace("{PLAYER}", e.getPlayer().getName()));
            e.getPlayer().teleport(ProjectInfected.getPlugin().getLobby());
            ScoreBoardUtils.createLobbyScoreboard(e.getPlayer());
        }
    }
}
