package me.xxmatthdxx.pi.listeners;

import me.xxmatthdxx.pi.game.GameManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

/**
 * Created by Matthew on 2015-05-17.
 */
public class BlockBreak implements Listener {
    
    @EventHandler
    public void onBreak(BlockBreakEvent e){
        Player pl = e.getPlayer();
        if(!pl.isOp()){
            e.setCancelled(false);
        }
        if(GameManager.getInstance().getAlive().contains(pl.getUniqueId()) || GameManager.getInstance().getInfected().contains(pl.getUniqueId())){
            e.setCancelled(true);
        }
        return;
    }
}
