package me.xxmatthdxx.pi.listeners;

import me.xxmatthdxx.pi.game.GameManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

/**
 * Created by Matthew on 2015-05-18.
 */
public class PlayerDamagePlayer implements Listener {

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent e) {
        if (!(e.getEntity() instanceof Player) && !(e.getDamager() instanceof Player)) {
            return;
        }

        Player pl = (Player) e.getEntity();
        Player damager = (Player) e.getDamager();

        if (GameManager.getInstance().getAlive().contains(pl.getUniqueId()) && GameManager.getInstance().getAlive().contains(damager.getUniqueId()) ||
                GameManager.getInstance().getInfected().contains(pl.getUniqueId()) &&
                        GameManager.getInstance().getInfected().contains(damager.getUniqueId())) {
            e.setCancelled(true);
        }

        if (pl.getHealth() - e.getDamage() <= 0) {
            pl.setHealth(pl.getMaxHealth());
            if (GameManager.getInstance().getInfected().contains(pl.getUniqueId())) {
                pl.teleport(GameManager.getInstance().getCurrentArena().getSpawn());
            } else if (GameManager.getInstance().getAlive().contains(pl.getUniqueId())) {
                GameManager.getInstance().addInfected(pl);
                if (GameManager.getInstance().canContinue()) {
                } else {
                    GameManager.getInstance().stop(true);
                }
            }
        }
        return;
    }
}
