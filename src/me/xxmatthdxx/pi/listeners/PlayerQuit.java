package me.xxmatthdxx.pi.listeners;

import me.xxmatthdxx.pi.ProjectInfected;
import me.xxmatthdxx.pi.game.GameManager;
import me.xxmatthdxx.pi.game.GameState;
import me.xxmatthdxx.pi.utils.MessageUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Random;

/**
 * Created by Matthew on 2015-05-17.
 */
public class PlayerQuit implements Listener {

    GameManager game = GameManager.getInstance();
    ProjectInfected plugin = ProjectInfected.getPlugin();
    MessageUtil msg = MessageUtil.getInstance();


    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player pl = e.getPlayer();
        e.setQuitMessage(msg.getMessageFromConfig("quit_message").replace("{PLAYER}", pl.getName()));

        if (Bukkit.getOnlinePlayers().size() <= plugin.getConfig().getInt("minPlayers")) {
            System.out.println("Stopping here due to 3 players");
            if(plugin.getState() == GameState.IN_GAME){
                game.stop(false);
            }
        }

        if (game.getInfected().contains(pl.getUniqueId())) {
            if (game.getInfected().size() < 1) {
                System.out.println("Infected size is less than 1");
                if (game.getAlive().size() >= 2) {
                    System.out.println("Alive is greater then 2");
                    Random ran = new Random();
                    game.addInfected(Bukkit.getPlayer(game.getAlive().get(ran.nextInt(game.getAlive().size()))));
                    game.getAlive().remove(game.getAlive().get(ran.nextInt(game.getAlive().size())));
                    game.getInfected().remove(pl.getUniqueId());
                    Bukkit.broadcastMessage(msg.getMessageFromConfig("team_balance"));
                }
                if (game.getAlive().size() == 0) {
                    System.out.println("Alive is 0");
                    game.stop(true);
                }
            }
            return;
        } else if (game.getAlive().contains(pl.getUniqueId())) {
            if (game.getAlive().size() <= 0) {
                if (game.getInfected().size() >= 1) {
                    game.stop(true);
                } else {
                    game.stop(false);
                }
            } else if(game.getAlive().size() >=2){
                if (game.getInfected().size() <= 0) {
                    Random ran = new Random();
                    game.addInfected(Bukkit.getPlayer(game.getAlive().get(ran.nextInt(game.getAlive().size()))));
                    game.getAlive().remove(pl.getUniqueId());
                    Bukkit.broadcastMessage(msg.getMessageFromConfig("team_balance"));
                }
                return;
            }
            game.getAlive().remove(pl.getUniqueId());
            return;
        }
    }
}
