package me.xxmatthdxx.pi.utils;

import me.xxmatthdxx.pi.ProjectInfected;
import org.bukkit.ChatColor;

/**
 * Created by Matthew on 2015-05-17.
 */
public class MessageUtil {

    private static MessageUtil instance = new MessageUtil();

    public static MessageUtil getInstance() {
        return instance;
    }

    public String getMessageFromConfig(String path) {
        return ChatColor.translateAlternateColorCodes('&', ProjectInfected.getPlugin().getConfig().getString(path));
    }
}
