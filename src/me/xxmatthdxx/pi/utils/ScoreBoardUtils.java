package me.xxmatthdxx.pi.utils;

import me.xxmatthdxx.pi.game.Arena;
import me.xxmatthdxx.pi.game.GameManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

/**
 * Created by Matt on 18/05/2015.
 */
public class ScoreBoardUtils {

    private static Scoreboard board;
    private static Objective objective;
    private static String name;

    public static void createLobbyScoreboard(Player player){
        board = Bukkit.getScoreboardManager().getNewScoreboard();
        name = MessageUtil.getInstance().getMessageFromConfig("lobby_sb_title");
        objective = board.registerNewObjective("test", "dummy");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName(name);
        for(Arena a : GameManager.getInstance().getArenas()){
            Score score = objective.getScore(a.getName());
            score.setScore(a.getVotes());
        }
        player.setScoreboard(board);
    }
}
