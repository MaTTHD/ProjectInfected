package me.xxmatthdxx.pi.game;

/**
 * Created by Matthew on 2015-05-17.
 */
public enum GameState {

    LOBBY, IN_GAME,OFFLINE, VOTING;
}
