package me.xxmatthdxx.pi.game;

import me.xxmatthdxx.pi.utils.ScoreBoardUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * Created by Matthew on 2015-05-17.
 */
public class Arena {

    private String name;
    private Location spawn;
    private Location p1, p2;
    private boolean isPlaying;
    private int votes;

    public Arena(String name, Location spawn, Location p1, Location p2){
        this.name = name;
        this.spawn = spawn;
        this.p1 = p1;
        this.p2 = p2;
        isPlaying = false;
        votes = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getSpawn() {
        return spawn;
    }

    public void setSpawn(Location spawn) {
        this.spawn = spawn;
    }

    public Location getP1() {
        return p1;
    }

    public void setP1(Location p1) {
        this.p1 = p1;
    }

    public Location getP2() {
        return p2;
    }

    public void setP2(Location p2) {
        this.p2 = p2;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean isPlaying) {
        this.isPlaying = isPlaying;
    }

    public int getVotes(){
        return votes;
    }

    public void addVote(){
        votes++;
        for(Player pl : Bukkit.getOnlinePlayers()){
            ScoreBoardUtils.createLobbyScoreboard(pl);
        }
    }

    public void removeVote(){
        votes--;
        for(Player pl : Bukkit.getOnlinePlayers()){
            ScoreBoardUtils.createLobbyScoreboard(pl);
        }
    }

    public void setVotes(int votes){
        this.votes = votes;
    }
}
