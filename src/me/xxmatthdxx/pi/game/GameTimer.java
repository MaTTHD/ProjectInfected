package me.xxmatthdxx.pi.game;

import me.xxmatthdxx.pi.ProjectInfected;
import me.xxmatthdxx.pi.utils.MessageUtil;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Matthew on 2015-05-17.
 */
public class GameTimer extends BukkitRunnable {

    private ProjectInfected plugin = ProjectInfected.getPlugin();
    private GameState state = plugin.getState();

    public final int VOTING_TIME = 45;
    public final int COUNT_TO_GAME = 10;
    public final int GAME_TIME = 8*60;

    private final MessageUtil msg = MessageUtil.getInstance();
    private final GameManager game = GameManager.getInstance();
    private static GameTimer instance = new GameTimer();
    int task;

    public static GameTimer getInstance(){
        return instance;
    }

    @Override
    public void run() {
        if(plugin.getTicks() > 0){
            plugin.setTicks(plugin.getTicks() - 1);
        }

        if(state == GameState.VOTING){
            if(plugin.getTicks() % 15 == 0 && plugin.getTicks() != 0){
                Bukkit.broadcastMessage(msg.getMessageFromConfig("vote_notification").replace("{TIME}", Integer.toString(plugin.getTicks())));
                return;
            }
            if(plugin.getTicks() < 5 && plugin.getTicks() != 0){
                Bukkit.broadcastMessage(msg.getMessageFromConfig("vote_ending").replace("{TIME}", Integer.toString(plugin.getTicks())));
                return;
            }
            else if(plugin.getTicks() == 0){
                if(Bukkit.getOnlinePlayers().size() < plugin.getConfig().getInt("minPlayers")){
                    Bukkit.broadcastMessage(msg.getMessageFromConfig("no_players"));
                    plugin.setTicks(VOTING_TIME);
                    return;
                }
                game.getMapChosen();
                Bukkit.broadcastMessage(msg.getMessageFromConfig("map_chosen").replace("{MAP}", game.getMapChosen()).replace("{VOTES}", Integer.toString(game.getCurrentArena().getVotes())));
                game.start();
                task = Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
                    public void run(){
                        game.chooseInfected(1);
                        Bukkit.getServer().getScheduler().cancelTask(task);
                    }
                }, 20 * 15L);
            }
        }
        if(state == GameState.IN_GAME){
            //SWAG
            if(plugin.getTicks() % 30 == 0 && plugin.getTicks() != 0){
                Bukkit.broadcastMessage(msg.getMessageFromConfig("game_end_notification").replace("{TIME}", Integer.toString(plugin.getTicks())));
                return;
            }
            if(plugin.getTicks() < 5 && plugin.getTicks() != 0){
                Bukkit.broadcastMessage(msg.getMessageFromConfig("game_end_notification").replace("{TIME}", Integer.toString(plugin.getTicks())));
                return;
            }
            else if(plugin.getTicks() == 0){
                if(game.getAlive().size() <= 0){
                    game.stop(true);
                }
            }
        }
        else {
            if(state == GameState.OFFLINE){
                //doshit
            }
        }
    }
}
