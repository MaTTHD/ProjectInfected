package me.xxmatthdxx.pi.game;

import me.libraryaddict.disguise.DisguiseAPI;
import me.libraryaddict.disguise.disguisetypes.DisguiseType;
import me.libraryaddict.disguise.disguisetypes.MobDisguise;
import me.xxmatthdxx.pi.ProjectInfected;
import me.xxmatthdxx.pi.utils.MessageUtil;
import me.xxmatthdxx.pi.utils.Title;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.*;

/**
 * Created by Matthew on 2015-05-17.
 */
public class GameManager {

    private static GameManager instance = new GameManager();
    private Arena currentArena;

    private ProjectInfected plugin = ProjectInfected.getPlugin();
    private MessageUtil msg = MessageUtil.getInstance();
    int task;


    public HashMap<UUID, Arena> getPlayerVotes() {
        return playerVotes;
    }

    private HashMap<UUID, Arena> playerVotes = new HashMap<>();

    public static GameManager getInstance() {
        return instance;
    }

    List<Arena> arenas = new ArrayList<>();
    List<UUID> alive = new ArrayList<>();
    List<UUID> infected = new ArrayList<>();

    public Arena getCurrentArena() {
        return currentArena;
    }

    public void start() {
        for (Player pl : Bukkit.getOnlinePlayers()) {
            pl.teleport(currentArena.getSpawn());
            pl.playSound(pl.getLocation(), Sound.LEVEL_UP, 1, 1);
            pl.setHealth(pl.getMaxHealth());
            pl.setFlying(false);
            pl.setAllowFlight(false);
            pl.setGameMode(GameMode.SURVIVAL);
            pl.getInventory().setItem(0, new ItemStack(Material.STONE_SWORD, 1));
            pl.getInventory().setHeldItemSlot(0);
            alive.add(pl.getUniqueId());
        }
        plugin.setState(GameState.IN_GAME);
        plugin.setTicks(GameTimer.getInstance().GAME_TIME);
    }

    public void stop(boolean winner) {
        getPlayerVotes().clear();
        if (winner) {

            ProjectInfected.getPlugin().setTicks(GameTimer.getInstance().VOTING_TIME);
            ProjectInfected.getPlugin().setState(GameState.VOTING);
            for (Arena a : arenas) {
                a.setVotes(0);
            }

            if (alive.size() <= 0) {
                //Infected win!
                Bukkit.broadcastMessage(msg.getMessageFromConfig("infected_win"));
                for (Player pl : Bukkit.getOnlinePlayers()) {
                    Title.sendTitle(pl, msg.getMessageFromConfig("infected_win_title"), msg.getMessageFromConfig("infected_win_subtitle"), 20, 20, 20);
                }
                for (UUID uuid : infected) {
                    Player pl = Bukkit.getPlayer(uuid);
                    infected.remove(pl);
                    DisguiseAPI.undisguiseToAll(pl);
                }
                for (Player pl : Bukkit.getOnlinePlayers()) {
                    pl.setHealth(pl.getMaxHealth());
                    pl.getInventory().clear();
                    pl.teleport(plugin.getLobby());
                }
                return;
            } else if (plugin.getTicks() == 0) {
                //Humans fucking win!
                Bukkit.broadcastMessage(msg.getMessageFromConfig("human_win"));

                for (UUID uuid : infected) {
                    Player pl = Bukkit.getPlayer(uuid);
                    infected.remove(pl);
                    DisguiseAPI.undisguiseToAll(pl);
                }
                for (Player pl : Bukkit.getOnlinePlayers()) {
                    pl.setHealth(pl.getMaxHealth());
                    pl.getInventory().clear();
                    pl.teleport(plugin.getLobby());
                }

                for (Player pl : Bukkit.getOnlinePlayers()) {
                    Title.sendTitle(pl, msg.getMessageFromConfig("human_win_title"), msg.getMessageFromConfig("human_win_subtitle"), 20, 20, 20);
                    return;
                }
            }
        } else {

            Bukkit.broadcastMessage(msg.getMessageFromConfig("game_force_stop"));

            for (UUID uuid : infected) {
                Player pl = Bukkit.getPlayer(uuid);
                infected.remove(pl);
                DisguiseAPI.undisguiseToAll(pl);
            }
            for (Player pl : Bukkit.getOnlinePlayers()) {
                pl.setHealth(pl.getMaxHealth());
                pl.getInventory().clear();
                pl.teleport(plugin.getLobby());
            }

            ProjectInfected.getPlugin().setTicks(GameTimer.getInstance().VOTING_TIME);
            ProjectInfected.getPlugin().setState(GameState.VOTING);
            for (Arena a : arenas) {
                a.setVotes(0);
            }
        }
        alive.clear();
        infected.clear();
    }

    public void setup() {
        if (plugin.getConfig().getString("lobby") == null) {
            Location loc = new Location(Bukkit.getWorld("Lobby"), 90.642, 17.00000, 1386.393);
            plugin.getConfig().set("lobby", loc.getX() + "," + loc.getY() + "," + loc.getZ() + "," + loc.getWorld().getName());
            plugin.saveConfig();
            plugin.reloadConfig();
        } else {
            String[] split = plugin.getConfig().getString("lobby").split(",");
            double x = Double.valueOf(split[0]);
            double y = Double.valueOf(split[1]);
            double z = Double.valueOf(split[2]);
            World world = Bukkit.getWorld(split[3]);
            Location loc = new Location(world, x, y, z);
            plugin.setLobby(loc);
        }

        if (plugin.getConfig().getConfigurationSection("arenas") == null) {
            plugin.getConfig().createSection("arenas");
            return;
        }

        for (String s : plugin.getConfig().getConfigurationSection("arenas").getKeys(false)) {
            String[] split = plugin.getConfig().getString("arenas." + s + ".spawn").split(",");
            String[] p1Split = plugin.getConfig().getString("arenas." + s + ".p1").split(",");
            String[] p2Split = plugin.getConfig().getString("arenas." + s + ".p2").split(",");

            Location spawn = new Location(Bukkit.getWorld(split[3]), Double.valueOf(split[0]), Double.valueOf(split[1]), Double.valueOf(split[2]));
            Location p1 = new Location(Bukkit.getWorld(p1Split[3]), Double.valueOf(p1Split[0]), Double.valueOf(p1Split[1]), Double.valueOf(p1Split[2]));
            Location p2 = new Location(Bukkit.getWorld(p2Split[3]), Double.valueOf(p2Split[0]), Double.valueOf(p2Split[1]), Double.valueOf(p2Split[2]));

            Arena a = new Arena(s, spawn, p1, p2);
            arenas.add(a);
            System.out.println("Loaded arena " + a.getName());
        }
    }

    public List<UUID> getInfected() {
        return infected;
    }

    public List<UUID> getAlive() {
        return alive;
    }

    public List<Arena> getArenas() {
        return arenas;
    }

    public String getMapChosen() {
        int current = 0;
        Arena toChose = arenas.get(0);
        for (Arena a : arenas) {
            if (a.getVotes() > current) {
                current = a.getVotes();
                toChose = a;
            }
        }
        currentArena = toChose;
        return toChose.getName();
    }

    public void chooseInfected(int amount) {
        infected.clear();
        System.out.println("Chooses infected.");
        Random ran = new Random();
        for (int i = 0; i < amount; i++) {
            Player pl = (Player) Bukkit.getOnlinePlayers().toArray()[ran.nextInt(Bukkit.getOnlinePlayers().size())];
            if (infected.contains(pl.getUniqueId())) {
                System.out.println("Contains player");
                continue;
            } else {
                System.out.println("Infects player");
                infected.add(pl.getUniqueId());
                alive.remove(pl.getUniqueId());
                Title.sendTitle(pl, msg.getMessageFromConfig("infected_title"), msg.getMessageFromConfig("infected_subtitle"), 20, 20, 20);
                Bukkit.broadcastMessage(msg.getMessageFromConfig("player_turns").replace("{PLAYER}", pl.getName()));
                MobDisguise mob = new MobDisguise(DisguiseType.PIG_ZOMBIE);
                mob.setViewSelfDisguise(false);
                DisguiseAPI.disguiseToAll(pl, mob);
            }
        }
    }

    public void addInfected(Player pl) {
        Title.sendTitle(pl, msg.getMessageFromConfig("infected_title"), msg.getMessageFromConfig("infected_subtitle"), 20, 20, 20);
        infected.add(pl.getUniqueId());
        alive.remove(pl.getUniqueId());
        Bukkit.broadcastMessage(msg.getMessageFromConfig("player_turns").replace("{PLAYER}", pl.getName()));
        MobDisguise mob = new MobDisguise(DisguiseType.PIG_ZOMBIE);
        mob.setViewSelfDisguise(false);
        DisguiseAPI.disguiseToAll(pl, mob);
    }

    public Arena getArena(String name) {
        for (Arena a : arenas) {
            if (a.getName().equalsIgnoreCase(name)) {
                return a;
            }
        }
        return null;
    }

    public boolean canContinue() {
        if (getAlive().size() >= 1) {
            return true;
        } else {
            return false;
        }
    }
}
