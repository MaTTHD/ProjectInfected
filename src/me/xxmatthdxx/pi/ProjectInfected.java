package me.xxmatthdxx.pi;

import me.libraryaddict.disguise.DisguiseAPI;
import me.xxmatthdxx.pi.cmds.CommandManager;
import me.xxmatthdxx.pi.game.GameManager;
import me.xxmatthdxx.pi.game.GameState;
import me.xxmatthdxx.pi.game.GameTimer;
import me.xxmatthdxx.pi.listeners.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.UUID;

/**
 * Created by Matthew on 2015-05-17.
 */
public class ProjectInfected extends JavaPlugin {

    private static ProjectInfected plugin;

    private int ticks;
    private GameState state;
    private Location spawn;
    private Location lobby;

    public File getPlayerDir() {
        return playerDir;
    }

    private File playerDir;

    public void onEnable(){
        plugin = this;

        if(!plugin.getDataFolder().exists()){
            getDataFolder().mkdir();

            plugin.getConfig().options().copyDefaults(true);
            saveDefaultConfig();
        }
        if(!new File(getDataFolder() + "/" + "playerData").exists()){
           new File(getDataFolder() + "/" + "playerData").mkdir();
        }
        playerDir = new File(getDataFolder() + "/" + "playerData");

        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new BlockBreak(), this);
        pm.registerEvents(new BlockPlace(), this);
        pm.registerEvents(new HungerEvent(), this);
        pm.registerEvents(new PlayerDamage(), this);
        pm.registerEvents(new PlayerJoin(), this);
        pm.registerEvents(new PlayerQuit(), this);
        pm.registerEvents(new PlayerDamagePlayer(), this);

        CommandManager cm = new CommandManager();
        getCommand("vote").setExecutor(cm);
        getCommand("game").setExecutor(cm);
        getCommand("stats").setExecutor(cm);

        for(UUID uuid : GameManager.getInstance().getInfected()){
            Player pl = Bukkit.getPlayer(uuid);
            DisguiseAPI.undisguiseToAll(pl);
        }

        setState(GameState.VOTING);
        setTicks(45);
        GameTimer.getInstance().runTaskTimer(this, 0L, 20L);
        GameManager.getInstance().setup();
        for(Player pl : Bukkit.getOnlinePlayers()){
            pl.teleport(lobby);
        }
    }
    public void onDisable(){
        plugin = null;
    }

    public static ProjectInfected getPlugin(){
        return plugin;
    }

    public GameState getState(){
        return state;
    }

    public void setState(GameState state){
        this.state = state;
    }

    public int getTicks(){
        return ticks;
    }

    public void setTicks(int ticks){
        this.ticks = ticks;
    }

    public Location getSpawn(){
        return spawn;
    }

    public Location getLobby(){
        return lobby;
    }

    public void setSpawn(Location loc){
        spawn = loc;
    }
    public void setLobby(Location loc){
        lobby = loc;
    }
}
